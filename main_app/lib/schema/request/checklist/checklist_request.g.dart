// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checklist_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChecklistData _$ChecklistDataFromJson(Map<String, dynamic> json) =>
    ChecklistData(
      name: json['name'] as String? ?? '',
      isFinished: json['isFinished'] as bool? ?? false,
    );

Map<String, dynamic> _$ChecklistDataToJson(ChecklistData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'isFinished': instance.isFinished,
    };
