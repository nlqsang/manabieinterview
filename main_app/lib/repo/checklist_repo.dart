import 'dart:convert';

import 'package:base_app/core/base_repo.dart';
import 'package:injectable/injectable.dart';
import 'package:main_app/schema/common/firestore_collections.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class IChecklistRepo extends BaseRepo {
  Future<bool> createChecklist(ChecklistData checklist);
  Future<List<ChecklistData>> retrieveChecklist();
  Future<bool> updateChecklist(ChecklistData data);
}

FirebaseFirestore firestore = FirebaseFirestore.instance;
CollectionReference checklists =
    FirebaseFirestore.instance.collection(COLLECTION_CHECKLIST);

@Singleton(as: IChecklistRepo)
class ChecklistRepoImpl implements IChecklistRepo {
  ChecklistRepoImpl();

  @override
  Future<bool> createChecklist(ChecklistData checklist) async {
    try {
      // returns TRUE if success
      await checklists.add(checklist.toMap());
      return true;
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<List<ChecklistData>> retrieveChecklist() async {
    try {
      var querySnapshot = await checklists.get();
      List<String> idList = [];

      // Get firebase document id
      for (var doc in querySnapshot.docs) {
        idList.add(doc.id);
      }

      // Map firebase document into ChecklistData
      var decodedList = querySnapshot.docs
          .map((doc) =>
              ChecklistData.fromJson(doc.data() as Map<String, dynamic>))
          .toList();

      // Bind ChecklistData with firebase document id
      for (var i = 0; i < decodedList.length; i++) {
        decodedList[i].id = idList[i];
      }

      return decodedList;
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<bool> updateChecklist(ChecklistData data) async {
    try {
      // returns TRUE if success
      await checklists.doc(data.id).update(data.toMap());
      return true;
    } catch (e) {
      throw Exception(e);
    }
  }
}
