import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/route/app_route.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';
import 'package:main_app/screen/checklist/checklist_main/checklist_main_controller.dart';
import 'package:main_app/screen/checklist/components/checklist_all_screen.dart';
import 'package:main_app/screen/checklist/components/checklist_complete_screen.dart';
import 'package:main_app/screen/checklist/components/checklist_incomplete_screen.dart';

class ChecklistMainScreen extends GetView<ChecklistMainController> {
  ChecklistMainScreen({Key? key}) : super(key: key);

  TextEditingController textController = TextEditingController();
  PageController _pageController = PageController(initialPage: 0);

  List<Widget> _bottomBars = <Widget>[
    ChecklistAllScreen(),
    ChecklistCompleteScreen(),
    ChecklistIncompleteScreen()
  ];

  void _onItemTapped(int index) {
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 400), curve: Curves.ease);
  }

  void _onOpenInputDialog() {
    Get.dialog(AlertDialog(
      title: Text("Create new item"),
      content: TextField(
        controller: textController,
        decoration: InputDecoration(hintText: "Wash my dishes"),
      ),
      actionsAlignment: MainAxisAlignment.end,
      actions: [
        TextButton(onPressed: _onCreateChecklistItem, child: Text("Create")),
      ],
    ));
  }

  void _onCreateChecklistItem() {
    if (textController.text == '') {
      Get.back();
      Get.snackbar("Don't be lazy!", "GIVE YOUR TASK A NAME!");
      return;
    }
    controller.createChecklist((ChecklistData(name: textController.text)));
    Get.back();
    textController.clear();
  }

  void _onPageChanged(int currentPage) {
    controller.currentIndex.value = currentPage;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 2,
        title: Text(
          'Checklist',
          style: _txtTitle,
        ),
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: _onPageChanged,
        children: _bottomBars,
      ),
      floatingActionButton: GestureDetector(
          onTap: _onOpenInputDialog,
          child: Container(
              width: 36,
              height: 36,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: ResColors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 4,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Align(
                child: Text(
                  "+",
                  textAlign: TextAlign.center,
                  style: _txtPlus,
                ),
                alignment: Alignment.center,
              ))),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      bottomNavigationBar: Obx(() => BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.checklist),
                label: 'All',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.done),
                label: 'Complete',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.incomplete_circle),
                label: 'Incomplete',
              ),
            ],
            currentIndex: controller.currentIndex.value,
            selectedItemColor: Colors.amber[800],
            onTap: _onItemTapped,
          )),
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: ResColors.contentLarge,
    fontFamily: ResFonts.sfTextMedium);

var _txtPlus = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w400,
    color: ResColors.clickMe,
    fontFamily: ResFonts.sfTextRegular);
