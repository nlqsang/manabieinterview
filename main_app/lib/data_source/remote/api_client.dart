import 'package:base_app/data_source/remote/base_api_client.dart';
import 'package:base_app/logger/app_logger.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:main_app/data_source/local/shared_preferences_repo.dart';
import 'package:main_app/flavor/flavor_config.dart';

@singleton
class ApiClient extends BaseApiClient {
  final SharedPreferencesRepo _sharedPreferencesRepo;

  late Dio dio;

  // late NotificationService notificationService;

  ApiClient(this._sharedPreferencesRepo) {
    AppLogger.d('Init dio api client');

    dio = getDio(
      endpoint: ApiEndpoint.END_POINT,
      timeOut: 5 * 60 * 1000,
    );

    // notificationService = NotificationService(dio);
  }

  @override
  Map<String, dynamic>? getHeaders() {
    var token = _sharedPreferencesRepo.getToken();
    AppLogger.d(token);

    return {
      'Authorization': 'Bearer $token',
    };
  }

  @override
  void onTokenExpired() {}
}

class ApiEndpoint {
  static final String END_POINT = FlavorConfig.apiEndPoint;
}
