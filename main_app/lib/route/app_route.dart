import 'package:get/get.dart';
import 'package:main_app/screen/checklist/checklist_main/checklist_main_controller.dart';
import 'package:main_app/screen/checklist/checklist_main/checklist_main_screen.dart';

const ROUTE_CHECKLIST_MAIN_SCREEN = '/ROUTE_CHECKLIST_MAIN_SCREEN';

var pages = [
  GetPage(
    name: ROUTE_CHECKLIST_MAIN_SCREEN,
    page: () => ChecklistMainScreen(),
    bindings: [ChecklistMainBinding()],
  ),
];
