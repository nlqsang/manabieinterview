import 'package:main_app/repo/checklist_repo.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

class MockChecklistRepo extends Mock implements ChecklistRepoImpl {}

void main() {
  final repo = MockChecklistRepo();
  var mockupChecklistData = ChecklistData();
  group('Checklist Retrieve', () {
    test('Empty', () async {
      when(() => repo.retrieveChecklist())
          .thenAnswer((realInvocation) async => await Future.value([]));
      expect(await repo.retrieveChecklist(), []);
    });

    test('Not Empty', () async {
      when(() => repo.retrieveChecklist()).thenAnswer((realInvocation) async =>
          await Future<List<ChecklistData>>.value([mockupChecklistData]));
      expect(await repo.retrieveChecklist(), [mockupChecklistData]);
    });

    // test('Exception', () async {
    //   when(() => repo.retrieveChecklist()).thenThrow('Firestore Exception');
    //   expect(await repo.retrieveChecklist(), 'Firestore Exception');
    // });
  });

  group('Checklist Create', () {
    test('Success', () async {
      when(() => repo.createChecklist(mockupChecklistData))
          .thenAnswer((invocation) => Future.value(true));
      expect(await repo.createChecklist(mockupChecklistData), true);
    });
  });

  group('Checklist Update', () {
    test('Success', () async {
      when(() => repo.updateChecklist(mockupChecklistData))
          .thenAnswer((invocation) => Future.value(true));
      expect(await repo.updateChecklist(mockupChecklistData), true);
    });
  });
}
