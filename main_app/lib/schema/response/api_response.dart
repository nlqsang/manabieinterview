import 'package:base_app/logger/app_logger.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:main_app/resource/res_lang.dart';
import 'package:main_app/schema/response/api_error.dart';
import 'package:get/get.dart';

part 'api_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class APIResponse<T> {
  // From API
  String? status;
  T? data;
  bool isSuccess;
  List<ApiError>? errors;

  // For local handel
  int? statusCode;
  String? message;

  APIResponse({
    this.status,
    this.data,
    this.statusCode = 200,
    this.message,
    this.isSuccess = true,
    this.errors,
  });

  factory APIResponse.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$APIResponseFromJson(json, fromJsonT);

  factory APIResponse.fromException(ex) {
    AppLogger.e(ex);

    var errorCode = 500;
    var errorMessage = ResLang.common_api_error.tr;

    if (ex is DioError) {
      // AppLogger.e(ex.message);
      AppLogger.d(ex.response?.data);

      switch (ex.type) {
        case DioErrorType.connectTimeout:
        case DioErrorType.sendTimeout:
        case DioErrorType.receiveTimeout:
          errorCode = 408;
          break;
        case DioErrorType.response:
          errorCode = ex.response?.statusCode ?? 500;
          break;
        case DioErrorType.cancel:
          errorCode = 499;
          break;
        case DioErrorType.other:
          errorCode = 500;
          break;
      }

      // Parse error message.
      try {
        var errorResponse = APIResponse.fromJson(
          ex.response?.data,
          (json) => json as dynamic,
        );

        // Get 1st error for now.
        if (errorResponse.errors != null &&
            errorResponse.errors?.isNotEmpty == true) {
          errorMessage = errorResponse.errors?[0].message ?? errorMessage;
        }
      } catch (e) {
        AppLogger.e(e);
      }
    }

    return APIResponse(
      statusCode: errorCode,
      isSuccess: false,
      message: errorMessage,
    );
  }
}
