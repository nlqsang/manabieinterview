import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/screen/checklist/checklist_main/checklist_main_controller.dart';
import 'package:main_app/widget/checklist/checklist_listview.dart';

class ChecklistIncompleteScreen extends GetView<ChecklistMainController> {
  ChecklistIncompleteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => ChecklistListview(
        items: RxList(controller.checkItems.value
            .where((e) => e.isFinished != true)
            .toList())));
  }
}
