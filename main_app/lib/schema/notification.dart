// import 'package:json_annotation/json_annotation.dart';
// import 'package:main_app/converter/local_datetime_converter.dart';

// part 'notification.g.dart';

// @JsonSerializable()
// @LocalDateTimeConverter()
// class Notification {
//   String? id;
//   DateTime? createdDate;
//   bool? isRead;
//   NotificationData? data;

//   Notification({
//     this.id,
//     this.createdDate,
//     this.isRead,
//     this.data,
//   });

//   factory Notification.fromJson(Map<String, dynamic> json) =>
//       _$NotificationFromJson(json);

//   Map<String, dynamic> toJson() => _$NotificationToJson(this);
// }

// @JsonSerializable()
// class NotificationData {
//   String? pushType;
//   String? action;
//   String? data;
//   String? recordId;
//   String? title;
//   String? body;

//   NotificationData({
//     this.pushType,
//     this.action,
//     this.data,
//     this.recordId,
//     this.title,
//     this.body,
//   });

//   factory NotificationData.fromJson(Map<String, dynamic> json) =>
//       _$NotificationDataFromJson(json);

//   Map<String, dynamic> toJson() => _$NotificationDataToJson(this);
// }
