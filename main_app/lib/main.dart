import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/di/injection.dart';
import 'package:main_app/flavor/flavor_config.dart';
import 'package:main_app/localization/localization_service.dart';
import 'package:main_app/resource/res_style.dart';
import 'package:main_app/route/app_route.dart';
import 'package:firebase_core/firebase_core.dart';

// Init lib here: locator, lang ...
void main() async => {
      WidgetsFlutterBinding.ensureInitialized(),
      await configureDependencies(),
      await Firebase.initializeApp(),
      FlavorConfig.appFlavor = Flavor.DEV,
      runApp(
        MyApp(),
      ),
    };

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'TM',
      getPages: pages,
      initialRoute: ROUTE_CHECKLIST_MAIN_SCREEN,
      locale: LocalizationService.locale,
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
      debugShowCheckedModeBanner: false,
      theme: appTheme,
    );
  }
}
