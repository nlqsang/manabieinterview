class ResDrawable {
  static const baseAsset = 'assets/images/';

  static const bgError = baseAsset + 'bg_error.svg';
  static const bgNoData = baseAsset + 'bg_no_data.png';
  static const bgChecklist = baseAsset + 'bg_checklist.jpg';
}
