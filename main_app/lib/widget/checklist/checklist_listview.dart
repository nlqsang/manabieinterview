import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';
import 'package:main_app/screen/checklist/checklist_main/checklist_main_controller.dart';
import 'package:main_app/widget/checklist/checklist_row.dart';

class ChecklistListview extends GetView<ChecklistMainController> {
  List<ChecklistData> items;

  ChecklistListview({required this.items});

  void _onToggleChecklist(ChecklistData data) {
    controller.updateChecklist(data);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: items.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 50,
              child: ChecklistRow(
                data: items[index],
                onChanged: (bool? value) => _onToggleChecklist(items[index]),
              ),
            );
          }),
    );
  }
}
