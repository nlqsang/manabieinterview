import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';

class ChecklistRow extends StatelessWidget {
  ChecklistData data;
  final void Function(bool? value) onChanged;

  ChecklistRow({required this.data, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return (Row(
      children: [
        Checkbox(value: data.isFinished, onChanged: onChanged),
        SizedBox(
          width: 8,
        ),
        Text(data.name)
      ],
    ));
  }
}
