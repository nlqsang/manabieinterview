enum Flavor { DEV, STG, PROD }

class FlavorConfig {
  static Flavor appFlavor = Flavor.DEV;

  static String get apiEndPoint {
    switch (appFlavor) {
      case Flavor.DEV:
        return 'your_url';

      case Flavor.STG:
        return 'your_url';

      case Flavor.PROD:
        return 'your_url';
    }
  }
}
