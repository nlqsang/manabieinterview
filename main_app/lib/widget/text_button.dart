import 'dart:async';

import 'package:flutter/material.dart';
import 'package:main_app/resource/res_font.dart';

class AppTextButton extends StatefulWidget {
  final String title;
  final void Function(BuildContext context) onPress;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final double borderRadius;
  final Color backgroundColor;
  final Image? leadingIcon;
  final Image? trailingIcon;
  final TextStyle textStyle;
  final bool disabled;
  final Color? borderColor;

  const AppTextButton(this.title, this.onPress,
      {Key? key,
      this.margin = const EdgeInsets.all(0),
      this.padding = const EdgeInsets.all(12),
      this.borderRadius = 6,
      this.leadingIcon,
      this.trailingIcon,
      this.textStyle = const TextStyle(
          fontSize: 18,
          fontFamily: ResFonts.sfTextRegular,
          fontWeight: FontWeight.w600,
          color: Colors.white),
      this.disabled = false,
      this.borderColor,
      this.backgroundColor = const Color(0xFFFF353C)})
      : super(key: key);

  @override
  _AppTextButtonState createState() => _AppTextButtonState();
}

class _AppTextButtonState extends State<AppTextButton> {
  var disabledAfterPress = false;

  StreamController _timerStream = new StreamController<int>();

  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    _timerStream.close();

    super.dispose();
  }

  void _onPress() {
    this.widget.onPress(context);
    _timerStream.add(2);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: this.widget.margin,
        child: StreamBuilder(
          stream: _timerStream.stream,
          builder: (BuildContext ctx, AsyncSnapshot snapshot) {
            return IgnorePointer(
              ignoring: this.widget.disabled || snapshot.data == 0,
              child: TextButton(
                style: ButtonStyle(
                  padding:
                      MaterialStateProperty.all<EdgeInsets>(EdgeInsets.zero),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          side: this.widget.borderColor == null
                              ? BorderSide(color: Colors.white)
                              : BorderSide(color: this.widget.borderColor!),
                          borderRadius:
                              BorderRadius.circular(this.widget.borderRadius))),
                  backgroundColor:
                      MaterialStateProperty.all(this.widget.backgroundColor),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (this.widget.leadingIcon != null) ...[
                      this.widget.leadingIcon!
                    ],
                    Flexible(
                      child: Padding(
                        padding: this.widget.padding,
                        child: Text(this.widget.title,
                            textAlign: TextAlign.center,
                            style: this.widget.textStyle.merge(TextStyle(
                                color: this.widget.disabled
                                    ? this
                                        .widget
                                        .textStyle
                                        .color!
                                        .withOpacity(.4)
                                    : this.widget.textStyle.color))),
                      ),
                    ),
                    if (this.widget.trailingIcon != null) ...[
                      this.widget.trailingIcon!
                    ]
                  ],
                ),
                onPressed: _onPress,
              ),
            );
          },
        ));
  }
}