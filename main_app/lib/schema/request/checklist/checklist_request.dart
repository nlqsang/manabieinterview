import 'package:json_annotation/json_annotation.dart';

part 'checklist_request.g.dart';

@JsonSerializable()
class ChecklistData {
  String userId;
  String id;
  String name;
  bool isFinished;

  ChecklistData(
      {this.userId = '',
      this.id = '',
      this.name = '',
      this.isFinished = false});

  factory ChecklistData.fromJson(Map<String, dynamic> json) =>
      _$ChecklistDataFromJson(json);

  Map<String, dynamic> toJson() => _$ChecklistDataToJson(this);

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'id': id,
      'name': name,
      'isFinished': isFinished,
    };
  }
}
