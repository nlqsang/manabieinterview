import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:main_app/repo/checklist_repo.dart';
import 'package:main_app/schema/request/checklist/checklist_request.dart';
import 'package:main_app/utils/extensions.dart';

class ChecklistMainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ChecklistMainController(
          GetIt.instance.get<IChecklistRepo>(),
        ));
  }
}

class ChecklistMainController extends GetxController {
  final IChecklistRepo _checklistRepo;

  Rx<int> currentIndex = 0.obs;
  RxList<ChecklistData> checkItems = RxList<ChecklistData>([]);

  ChecklistMainController(this._checklistRepo);

  @override
  void onInit() async {
    // TODO: implement onInit
    await this.retrieveChecklist();
    super.onInit();
  }

  String talk() {
    return 'hello';
  }

  Future<void> createChecklist(ChecklistData checklist) async {
    // Create new checklist on firestore
    await this._checklistRepo.createChecklist(checklist);

    // We must retrieve the list in order to get the new document's id
    this.retrieveChecklist();
  }

  Future<List<ChecklistData>> retrieveChecklist() async {
    var checklistData = await this._checklistRepo.retrieveChecklist();
    this.checkItems.value = checklistData;

    return checklistData;
  }

  Future<void> updateChecklist(ChecklistData checklist) async {
    // Update checklist on firestore
    checklist.isFinished = !checklist.isFinished;
    await this._checklistRepo.updateChecklist(checklist);

    // Update item from local state to avoid retrieve the data multiple times
    var modifiedIndex = checkItems.indexOf(checklist);
    checkItems.update(modifiedIndex, checklist);
  }
}
